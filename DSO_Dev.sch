EESchema Schematic File Version 4
LIBS:DSO_Dev-cache
EELAYER 29 0
EELAYER END
$Descr B 17000 11000
encoding utf-8
Sheet 1 25
Title "DIGITAL STORATE MULTISIGNAL OSCILLOSCOPE"
Date ""
Rev ""
Comp "INTEGRATED MICROSYSTEM ELECTRONICS, LLC"
Comment1 "TITLE PAGE AND TABLE OF CONTENTS"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 14650 650  1550 150 
U 5D1E23BA
F0 "Power Management - Battery/USB Input" 50
F1 "DSO_Dev_Sheet02.sch" 50
$EndSheet
$Sheet
S 14650 1000 1550 150 
U 5D1E2592
F0 "Pwr Mgmt - Battery Charger/BMS" 50
F1 "DSO_Dev_Sheet03.sch" 50
$EndSheet
$Sheet
S 14650 1350 1550 150 
U 5D1E280F
F0 "Pwr Mgmt - DC/DC 5V/LED/-5V" 50
F1 "DSO_Dev_Sheet04.sch" 50
$EndSheet
$Sheet
S 14650 1700 1550 150 
U 5D1E29A7
F0 "Pwr Mgmt - DC/DC 3V3/2V5/1V8" 50
F1 "DSO_Dev_Sheet05.sch" 50
$EndSheet
$Sheet
S 14650 2050 1550 150 
U 5D1E30D9
F0 "Pwr Mgmt - DC/DC 1V2/JTAG" 50
F1 "DSO_Dev_Sheet06.sch" 50
$EndSheet
$Sheet
S 14650 2400 1550 150 
U 5D1E3260
F0 "Pwr Monitoring - Sensors" 50
F1 "DSO_Dev_Sheet07.sch" 50
$EndSheet
$Sheet
S 14650 2750 1550 150 
U 5D1E33E8
F0 "Pwr Monitoring - ADC" 50
F1 "DSO_Dev_Sheet08.sch" 50
$EndSheet
$Sheet
S 14650 3100 1550 150 
U 5D1E3539
F0 "Clock Generation" 50
F1 "DSO_Dev_Sheet09.sch" 50
$EndSheet
$Sheet
S 14650 3450 1550 150 
U 5D1E3611
F0 "Microcontroller" 50
F1 "DSO_Dev_Sheet10.sch" 50
$EndSheet
$Sheet
S 14650 3800 1550 150 
U 5D1E371D
F0 "Microcontroller - Periph Exp" 50
F1 "DSO_Dev_Sheet11.sch" 50
$EndSheet
$Sheet
S 14650 4150 1550 150 
U 5D1E3827
F0 "FPGA - ADC Input/Display Output" 50
F1 "DSO_Dev_Sheet12.sch" 50
$EndSheet
$Sheet
S 14650 4500 1550 150 
U 5D1E38ED
F0 "FPGA - DDR Interface" 50
F1 "DSO_Dev_Sheet13.sch" 50
$EndSheet
$Sheet
S 14650 4850 1550 150 
U 5D1E3A29
F0 "Memory - DDR3" 50
F1 "DSO_Dev_Sheet14.sch" 50
$EndSheet
$Sheet
S 14650 5200 1550 150 
U 5D1E3B09
F0 "Memory - EMMC/SD Card/Config PROM" 50
F1 "DSO_Dev_Sheet15.sch" 50
$EndSheet
$Sheet
S 14650 5550 1550 150 
U 5D1E3BE3
F0 "Human Interfaces - Switches/Display" 50
F1 "DSO_Dev_Sheet16.sch" 50
$EndSheet
$Sheet
S 14650 5900 1550 150 
U 5D1E3CD8
F0 "AFE - Channel A & B Attenuation" 50
F1 "DSO_Dev_Sheet17.sch" 50
$EndSheet
$Sheet
S 14650 6250 1550 150 
U 5D1E3E3F
F0 "AFE - Channel C & D Attenuation" 50
F1 "DSO_Dev_Sheet18.sch" 50
$EndSheet
$Sheet
S 14650 6600 1550 150 
U 5D1E3F8D
F0 "AFE - Channel A & B Conditioning" 50
F1 "DSO_Dev_Sheet19.sch" 50
$EndSheet
$Sheet
S 14650 6950 1550 150 
U 5D1E40C7
F0 "AFE - Channel C & D Conditioning" 50
F1 "DSO_Dev_Sheet20.sch" 50
$EndSheet
$Sheet
S 14650 7300 1550 150 
U 5D1E4631
F0 "ADC Channel A/Digital Port E" 50
F1 "DSO_Dev_Sheet21.sch" 50
$EndSheet
$Sheet
S 14650 7650 1550 150 
U 5D1E4724
F0 "ADC Channel B/DDS DAC" 50
F1 "DSO_Dev_Sheet22.sch" 50
$EndSheet
$Sheet
S 14650 8000 1550 150 
U 5D1E481C
F0 "ADC Channel C" 50
F1 "DSO_Dev_Sheet23.sch" 50
$EndSheet
$Sheet
S 14650 8350 1550 150 
U 5D1E4921
F0 "ADC Channel D" 50
F1 "DSO_Dev_Sheet24.sch" 50
$EndSheet
$Sheet
S 14650 8700 1550 150 
U 5D1E4A24
F0 "DDS Signal Conditioning and Output" 50
F1 "DSO_Dev_Sheet25.sch" 50
$EndSheet
$EndSCHEMATC
